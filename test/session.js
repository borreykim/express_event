/*! 
  \file session.js
  
  \author borrey
  
  Date Created: 2013-09-10T16:11:27-0600\n
  Date Modified:
  
  Copyright  All Rights Reserved  
*/
(function( Class, Store ) {
    var singleton = null,
    SessionStore  = function SessionStore(options){
	Store.call(this, options);
    }, Module = Class.extend({
	init : function( events, options ){
	    this.options = options;
	    this.storage = 'memory';
	    this.memory_store = {};
	    events.on('setupSession', function( data ){
		singleton.setupSession( data, options.secret, options.key );
	    });
	}, setupSession : function( data, secret, key ){
	    var express = data.express, app = data.app, store = new SessionStore({});
	    if( secret && key ){
		app.configure(function(){
		    app.use(express.session({
			store : store, secret : secret, key : key
		    }));
		});
	    }else{
		app.configure(function(){
		    app.use(express.session());
		});
	    }
	}, get : function( sid, fn ){
	    /*
	     * @param {String} sid
	     * @param {Function} fn
	     */
	    if( this.storage === 'memory' ){
		fn( null, this.memory_store[ sid ], null );
	    }else{
		fn( null, null );//err, session, extra
	    }
	}, set : function( sid, session, fn ){
	    /*
	     * @param {String} sid
	     * @param {Session} session
	     * @param {Function} fn
	     */
	    session[this.options.key] = sid;
	    if ( session && session.cookie && session.cookie.expires) {
		session.expires = Date.parse(session.cookie.expires);
	    }
	    //TODO: save session
	    if( this.storage === 'memory' ){
		this.memory_store[ sid ] = session;
	    }
	    if(fn){ fn( null, session ); }
	}, destroy : function( sid, fn ){
	    /*
	     * @param {String} sid
	     * @param {Function} fn
	     */
	    if( this.storage === 'memory' ){
		delete this.memory_store[ sid ];
	    }
	    if( fn ){ fn( null, null ); }
	}

    });

    SessionStore.prototype.__proto__ = Store.prototype;
    SessionStore.prototype.get = function (sid, callback ) { 
	var fn = typeof(callback) === 'function' ? callback : function () { };
	singleton.get( sid, fn );
    };
    SessionStore.prototype.set = function (sid, sess, callback ) {
	var fn = typeof(callback) === 'function' ? callback : function () { };
	singleton.set( sid, sess, fn );
    };
    SessionStore.prototype.destroy = function(sid, callback ){
	var fn = typeof(callback) === 'function' ? callback : function () { };
	singleton.destroy( sid, fn );		
    };



    exports.getModule = function( event, options ){
	if( !singleton ){
	    singleton = new Module( event, options );
	}
	return singleton;
    };
})( 
    require('ejohn-classjs'),
    require('connect').session.Store
  );
