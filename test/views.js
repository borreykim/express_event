/*! 
  \file session.js
  
  \author borrey
  
  Date Created: 2013-09-10T16:11:27-0600\n
  Date Modified:
  
  Copyright  All Rights Reserved  
*/
(function( Class ) {
    var singleton = null,
    Module = Class.extend({
	init : function( events, options ){

	    events.on('setupRoutes', function( data ){
		singleton.setupRoutes( data );
	    }).on('setupViews', function( data ){
		singleton.setupViews( data );
	    });

	},
	setupRoutes : function( data ){
	    var app = data.app, express = data.express;
	    app.configure('development', function(){
		app.use(require('stylus').middleware({ src:'../public' }));
		app.use( app.router );
		app.use( express.static('../public') );
		console.error('development');
	    });
	    app.configure('production', function(){
		/*use dist created by r.js to optimize javascript and css*/
		app.use(require('stylus').middleware({ src:'../public' }));
		app.use( app.router );
		app.use( express.static('../public') );
	    });
	    app.get('/', function(req, res){
		res.render('index', { title : 'Example' } );
	    });
	},
	setupViews : function( data ){
	    var app = data.app
	    app.configure(function(){
		app.set('views', __dirname + '/views');
		app.set('view engine', 'jade');
	    });
	}

    });

    exports.getModule = function( event, options ){
	if( !singleton ){
	    singleton = new Module( event, options );
	}
	return singleton;
    };
})( require('ejohn-classjs') );
