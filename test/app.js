/*! 

        \file app.js
  
        \author borrey
 
        Date Created: 2013-09-10T13:49:51-0600\n
        Date Modified:
 
        Copyright  All Rights Reserved  
*/
(function( fs, args, app ) {
    
    
    var opt = args.create([
	['L','config=ARG','Configuration file']
    ]).bindHelp().parseSystem(), 
    config_file = opt.options.config || __dirname+'/config_example.json';
    try{
	fs.readFile( config_file, 'utf8', function (err,data) {
	    if( err ){
		console.error('Error opening reading config file: ', config_file );
		process.exit(1);
	    }
	    var 
	    main = app.getApp( JSON.parse( data ) );//create express server
	    main.setupModules(); //go through config modules
	    main.setupPort(); //configure views
	    main.setupViews(); //configure views
	    main.setupInputParser( ); //ensure input parsers from requests
	    main.setupSession( ); //goes through setup for sessions
	    main.setupRoutes( ); //setup where to look for files
	    main.setupError( ); //any err handling that needs to be done
	    main.start(); //start listening

	});
    }catch( e ){
	console.error('Error on startup', e);
	process.exit();
    }
    process.on('uncaughtException', function (exception) {
	console.error( 'uncaughtException', exception );
    });
})( 
    require('fs'),
    require('node-getopt'),
    require('../lib/main') 
);

/**
 * Module dependencies.

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);
app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
 */
