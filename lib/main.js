/*! 

        \file main.js
  
        \author borrey
 
        Date Created: 2013-09-10T12:04:24-0600\n
        Date Modified:
 
        Copyright  All Rights Reserved  
*/
(function( express, Class, Events ) {
    var 
    singleton = null,
    Framework = Class.extend({
	init : function( config ){
	    this.events = new Events();
	    this.config = config || {};
	    this.modules = {};
	    this.app = express();
	    console.error( 'using:', config );
	},
	setupModules : function(  ){
	    var module_config = this.config.module || {},
	    module_name;
	    for( module_name in module_config ){
		this.modules[ module_name ] = this._loadModule( module_name, module_config[ module_name ] || {} );
	    }
	},
	_loadModule : function( module_name, options ){
	    /*
	      In the config each module should have it's own options.
	      needs option.path
	    */
	    try{
		var path = options.path || module_name;
		return require( path ).getModule( singleton.events, options );
	    }catch( e ){
		console.error('could not load module: ', module_name, options ,e);
		throw e;
	    }
	},
	setupPort : function(){
	    this.app.configure(function(){
		singleton.app.set('port', singleton.config.port || process.env.PORT || 3000);
	    });
	},
	setupViews : function( view_engine ){
	    this.events.emit('setupViews', { app : singleton.app });
	},
	setupInputParser : function(){
	    /*
	      Note: 
	      if you want to simulate DELETE and PUT, methodOverride is for that. 
	      If you pass in the _method post parameter set to 'delete' or 'put', 
	      then you can use app.delete and app.put in Express instead of using app.post all the time 
	      (thus more descriptive, verbose)
	      i.e.:
	      server:
	      app.put('/users/:id', function (req, res, next) {
	      // edit your user here
	      });
	      client:
	      <form> ...
	      <input type="hidden" name="_method" value="put" />
	      </form>
	    */
	    this.app.configure(function(){
		singleton.app.use( express.bodyParser() );
		singleton.app.use( express.methodOverride() );
		singleton.app.use( express.cookieParser() );
	    });
	},
	setupSession : function(){
	    this.events.emit('setupSession', { app : singleton.app, express : express });
	},
	setupRoutes : function(){
	    this.events.emit('setupRoutes', { app : singleton.app, express : express });
	},
	setupError : function(){
	    this.app.use(function( req, res, next ){
		res.send(404, 'Not Found');
	    });
	    this.app.use(function(err, req, res, next){
		console.error('Error: ', err, err.stack);
		res.send(500, 'Something broke!');
	    });
	    this.app.configure('development', function(){//>export NODE_ENV=development
		singleton.app.use( express.errorHandler({ dumpExceptions: true, showStack: true }) );
	    });
	    this.app.configure('production', function(){//>export NODE_ENV=production 
		singleton.app.use( express.errorHandler() );
	    });
	},
	start : function(){
	    var http = require('http');
	    http.createServer( singleton.app ).listen( singleton.app.get('port'), function(){
		singleton.events.emit('appStart');
		console.log("Express server listening on port " + singleton.app.get('port'));
	    });
	}
    });

    exports.getApp = function( config ){
	if( !singleton ){
	    singleton = new Framework( config );
	}
	return singleton;
    };

})( require('express'), require('ejohn-classjs'), require('event_amd') );
